#!/bin/bash
#part toYearWeek
BIN_CLICKHOUSE_CLIENT="/usr/bin/clickhouse-client"
DB_NAME="zabbix"
TABLE_NAME="trends"
CLICK_USER="default"
CLICK_PASSWORD="zabbix"
CLICK_HOST="192.168.0.112"
#104 week or 2 year
let SEC_TILL=`date +%s`-104*7*24*60*60
#get part list
PART_LIST=`$BIN_CLICKHOUSE_CLIENT --host $CLICK_HOST --user $CLICK_USER --password $CLICK_PASSWORD --query "SELECT partition FROM system.parts WHERE table like '%$TABLE_NAME%' AND database='$DB_NAME' AND active=1 AND partition<=toString(toYearWeek(toDateTime('$SEC_TILL')));" | awk '{print $1}'`
#drop part list
for i in $PART_LIST
do
  $BIN_CLICKHOUSE_CLIENT --host $CLICK_HOST --user $CLICK_USER --password $CLICK_PASSWORD --query "ALTER TABLE $DB_NAME.$TABLE_NAME DROP PARTITION '$i';"
done